# Adapted from https://rbvi.github.io/chimerax-recipes/copycrds/copycrds.html
# subdomain_transforms #3 to #4 vector_scale 3 arrowhead_radius 1

from scipy.spatial.transform import Rotation as R
from chimerax.core.commands import run
import numpy as np
import re

def subdomain_transforms(session, from_atoms, to_atoms=None, vector_scale=20, 
                      com_radius=10,
                      radius=4, arrowhead_radius=6, arrowhead_height=6,
                      rotvec_radius=6, rotvec_len=30, rotvec_visual_scale=None,
                      molmap_res=20, molmap_transparency=85, molmap_level=0.01,
                      model_id=None, color=None):
    from chimerax.core.errors import UserError

    ####################################
    # Get a free model number
    ####################################
    
    if model_id == None:
        cyl_modelid = next_free_model_id(session, 2300)
    else:
        cyl_modelid = model_id

    # info = run(session, 'info residues %s attribute ribbon_color' % from_atoms)
    if color == None:
        from chimerax.core.colors import hex_color
        color = run(session, 'get_chain_colors %s' % from_atoms, log=False)
        color = hex_color(color)
            
    cyl_modelname = get_model_name(session, from_atoms)

    ####################################
    # Compute rotation axis from (aligned) model pairs
    ####################################

    chain_res_spec = re.sub('^[#][0-9]+','',from_atoms)

    to_model = get_model_ids(session, from_atoms)[0]
    from_model = get_model_ids(session, from_atoms)[0]
    to_chain = get_chain_id(session, from_atoms)
    from_chain = get_chain_id(session, from_atoms)

    # Make temporary model for alignment measurements
    run(session, 'combine %s model #%d' % (from_atoms, cyl_modelid),
        log=False)
    print('align #%s%s to %s' % (cyl_modelid, chain_res_spec, to_atoms))
    run(session, 'align #%s%s to %s' % (cyl_modelid, chain_res_spec, to_atoms))

    # run(session, 'del #%d&~#%s/%s:%s' % \
    #     (cyl_modelid, cyl_modelid, from_chain, 
    #      ','.join([str(item) for item in get_resnums(session, from_atoms)])),
    #     log=False)

    # run(session, 'del #%d&~#%s%s' % \
    #     (cyl_modelid, cyl_modelid, chain_res_spec), 
    #     log=False)

    ###########################
    # Get rotation angle 
    ###########################

    symm_unitvec, symm_twist, symm_dist, symm_origin, symm_rot_matrix = \
        run(session, 'get_align_info #%s%s model2 %s' % (cyl_modelid, chain_res_spec, from_atoms))

    # Alternate way of getting the rotation axis, for checking
    # rot_axis, axis_angle, axis_shift = \
    #     get_chimera_rot_axis(session, from_atoms,
    #                          '#%d%s' % (cyl_modelid,chain_res_spec))
    # print('Alternate xform values:', axis_angle, axis_shift, symm_twist, symm_dist)    
    
    run(session, 'close #%d' % (cyl_modelid), log=False)

    ####################################
    # Make vectors in the direction of center-of-mass movement
    ####################################

    # Make new model using dummy atom 
    model_list1 = [m.id for m in session.models]
    run(session, 'build start atom TMP position 0,0,0 resname TMP', log=False)
    model_list2 = [m.id for m in session.models]
    temp_model_id = (list(set(model_list2) - set(model_list1))[0][0])

    run(session, 'color #%d %s atoms' % (temp_model_id, color), log=False)
    run(session, 'color #%d %s' % (temp_model_id, color), log=False)

    run(session, 'rename #%d "%s xform" id %d' % (temp_model_id, from_atoms[1:], cyl_modelid), log=False)
    # We should hide this dummy atom, but that changes the model color! (Chimerax bug?)
    # run(session, '~show #%d atoms' % (cyl_modelid))
    
    run(session, 'measure center %s mark true color %s radius %d model #%d.1' %
        (from_atoms, color, radius, cyl_modelid), log=False)
    run(session, 'measure center %s mark true color %s radius %d model #%d.2' %
        (to_atoms, color, radius, cyl_modelid), log=False)
    run(session, 'size #%d.1-2 atomradius %f' % (cyl_modelid, com_radius), log=False)
    
    
    run(session, '~show #%d.1-2 m' % (cyl_modelid), log=False)

    pos1 = run(session, 'getcrd #%d.1' % (cyl_modelid), log=False)
    pos1 = pos1[0]
    pos2 = run(session, 'getcrd #%d.2' % (cyl_modelid), log=False)
    pos2 = pos2[0]

    delta = pos2.copy() - pos1.copy()

    l_delta = np.sqrt(delta[0]**2 + delta[1]**2 + delta[2]**2)
    this_arrowhead_height = np.min(np.array([arrowhead_height, vector_scale * l_delta]))

    # Set bfactors to distance and angle values, for coloring
    run(session, 'setattr #%d.1 atoms bfactor %f' % (cyl_modelid, abs(l_delta)), log=False)
    run(session, 'setattr #%d.2 atoms bfactor %f' % (cyl_modelid, abs(symm_twist)), log=False)
    
    ####################################
    # Draw COM vector
    ####################################
    
    cyl_start = pos1.copy()
    cyl_end = cyl_start.copy() + \
        (vector_scale*l_delta - this_arrowhead_height) / \
        (vector_scale*l_delta) * \
        vector_scale*delta
    run(session,
        'shape cylinder from %f,%f,%f to %f,%f,%f '
        'radius %f model %s.3 color %s' % \
        (cyl_start[0], cyl_start[1], cyl_start[2],
         cyl_end[0], cyl_end[1], cyl_end[2],
         radius, cyl_modelid, color), log=False)

    cone_start = cyl_end.copy()
    cone_end = cone_start.copy() + this_arrowhead_height * delta/l_delta
    run(session,
        'shape cone fromPoint %f,%f,%f toPoint %f,%f,%f '
        'radius %f model %s.4 color %s' % \
        (cone_start[0], cone_start[1], cone_start[2],
         cone_end[0], cone_end[1], cone_end[2],
         arrowhead_radius, cyl_modelid, color), log=False)

    run(session, 'rename #%d.3 "COM shift (A): %6.2f"' % \
        (cyl_modelid, l_delta))
    run(session, 'rename #%d.4 "COM shift (A): %6.2f"' % \
        (cyl_modelid, l_delta))
    
    ####################################
    # Draw rotation axis
    ####################################
    
    cyl_start = pos1.copy() - rotvec_len/2*symm_unitvec[1]
    cyl_end = pos1.copy() + rotvec_len/2*symm_unitvec[1]

    if rotvec_visual_scale != None:
        rotvec_radius *= rotvec_visual_scale * symm_twist
        
    run(session,
        'shape cylinder from %f,%f,%f to %f,%f,%f '
        'radius %f model %s.5 color %s' % \
        (cyl_start[0], cyl_start[1], cyl_start[2],
         cyl_end[0], cyl_end[1], cyl_end[2],
         rotvec_radius, cyl_modelid, color), log=False)

    run(session, 'rename #%d.5 "rotation: %6.2f"' % \
        (cyl_modelid, symm_twist))

    ####################################
    # make colored volume
    ####################################
    molmap_model_id = next_free_model_id(session, 2300)
    model_list1 = [m.id for m in session.models]
    run(session, 'molmap %s %f gridspacing %f' % (from_atoms, molmap_res, molmap_res/10))
    model_list2 = [m.id for m in session.models]
    temp_model_id = (list(set(model_list2) - set(model_list1)))

    if len(temp_model_id) == 0:
        raise UserError("Error: there is a pre-existing molmap of %s; this script will be confused"
                        "unless you delete it." % from_atoms)
    else:
        temp_model_id = temp_model_id[0][0]
        
    run(session, 'color #%s %s surface' % (temp_model_id, color))
    run(session, 'vol #%s level %f' % (temp_model_id, molmap_level))
    run(session, 'transparency #%s %f surface' % (temp_model_id, molmap_transparency))
    
    run(session, 'rename #%d id #%d.6' % \
        (temp_model_id, cyl_modelid))
    # run(session, 'rename #%d id #%d.6' % \
    #     (molmap_model_id, cyl_modelid))
            
    return()
            
    #####################
    # Below code has not been tested
    #####################
    
    # fi_set = set(from_info)
    # ti_set = set(to_info)
    # if fi_set != ti_set:
    #     # different atoms and/or residues
    #     differences = fi_set ^ ti_set
    #     raise UserError("The two structures have different atoms (e.g. %s)" % differences.pop())

    # # have the same atoms but in a different order -- assign coordinates "by hand"
    # to_atom_lookup = {}
    # for to_string, to_atom in zip(to_info, to_atoms):
    #     to_atom_lookup[to_string] = to_atom

    # print('lookup')
    # for from_string, from_atom in zip(from_info, from_atoms):
    #     # to_atom_lookup[from_string].coord = from_atom.coord
    #     print(to_atom_lookup[from_string].scene_coord - from_atom.scene_coord)

def next_free_model_id(session, id):
    
    model_ids = get_model_ids(session)
    # Turn the model id list into a list of integers of the primary model IDs
    model_ids = [int(re.sub('^[#]','', re.sub('[.].*','', id))) for id in model_ids]

    next_model = id
    while next_model in model_ids:
        print('skipping', next_model)
        next_model += 1

    return(next_model)
    
def get_model_ids(session, spec_string='#*'):
    # See https://www.cgl.ucsf.edu/chimerax/docs/devel/genindex.html
    #  to find module to import from!
    from chimerax.list_info.cmd import info_selection
    from chimerax.list_info.cmd import info_models
    import json
    
    sel = run(session, 'sel %s' % spec_string, log=False)
    json_result_string = info_selection(session, return_json=True, level='model', attribute='model_id')
    json_result = json.loads(json_result_string.json_value)
    
    # Here we replace atom specs that begin i.e. with '#!3' (no submodels) with '#3';
    #  this doesn't seem to matter and it can mess up things later
    
    # models = [None] * len(json_result)
    models = [re.sub('[#][!]','#',dict['spec']) for dict in json_result]
    run(session, 'wait 1; ~sel sel', log=False)

    return(models)


def get_model_name(session, spec_string):
    # See https://www.cgl.ucsf.edu/chimerax/docs/devel/genindex.html
    #  to find module to import from!
    from chimerax.list_info.cmd import info_selection
    from chimerax.list_info.cmd import info_models
    import json
    
    sel = run(session, 'sel %s' % spec_string, log=False)
    json_result_string = info_selection(session, return_json=True, level='model', attribute='name')
    json_result = json.loads(json_result_string.json_value)

    names = [dict['value'] for dict in json_result]

    return(names[0])


def get_chain_id(session, spec_string):
    # See https://www.cgl.ucsf.edu/chimerax/docs/devel/genindex.html
    #  to find module to import from!
    from chimerax.list_info.cmd import info_selection
    from chimerax.list_info.cmd import info_models
    import json
    
    sel = run(session, 'sel %s' % spec_string, log=False)
    json_result_string = info_selection(session, return_json=True, level='chain', attribute='chain_id')
    json_result = json.loads(json_result_string.json_value)

    chain_ids = [dict['value'] for dict in json_result]

    return(chain_ids[0])


def get_resnums(session, spec_string):
    # See https://www.cgl.ucsf.edu/chimerax/docs/devel/genindex.html
    #  to find module to import from!
    from chimerax.list_info.cmd import info_selection
    from chimerax.list_info.cmd import info_models
    import json
    
    sel = run(session, 'sel %s' % spec_string, log=False)
    json_result_string = info_selection(session, return_json=True, level='residue', attribute='number')
    json_result = json.loads(json_result_string.json_value)

    resnums = [dict['value'] for dict in json_result]

    return(resnums)


def get_chain_colors(session, from_atoms):

    from_res = from_atoms.residues
    # Don't use non-polymer residue colors
    from chimerax.atomic import Residue
    from_res = from_res[from_res.polymer_types != Residue.PT_NONE]
    # chain_colors = {cid:color for cid,color in zip(from_res.chain_ids, from_res.ribbon_colors)}
    # return(chain_colors)

    chain_color = from_res.ribbon_colors[0]
    return(chain_color)


def get_align_info(session, model1, model2=None):
    # See below for where to import commands from:
    # https://www.cgl.ucsf.edu/chimerax/docs/devel/genindex.html
    
    from chimerax.std_commands.align import align

    # from chimerax.std_commands.view import view_matrix
    
    # https://www.cgl.ucsf.edu/chimerax/docs/devel/modules/core/commands/user_commands.html#chimerax.std_commands.align.align    
    a = align(session, model1, to_atoms=model2, report_matrix=True, move=False)

    xform_matrix = a[4].matrix
    rot_matrix = xform_matrix[0:3,0:3]
    shift_vec = xform_matrix[0:3,3]

    r = R.from_matrix(rot_matrix)
    rot_vec = r.as_rotvec(degrees=False)
    # eulers = r.as_euler('XZX', degrees=True)

    rot_unitvec = np.zeros([2,3])
    rot_unitvec[1,:] = rot_vec/np.linalg.norm(rot_vec)

    # Negative seems to be needed to match ChimeraX conventions..
    rot_angle = -180/np.pi * np.linalg.norm(rot_vec)
    rot_shift = np.linalg.norm(np.dot(shift_vec,rot_vec/np.linalg.norm(rot_vec)))
    # Get the rotation origin (see https://stackoverflow.com/questions/74519927/best-way-to-rotate-and-translate-a-set-of-points-in-python)
    # rot_origin = r.inv().apply(shift_vec)
    rot_origin = r.apply(shift_vec)
    
    return(rot_unitvec, rot_angle, rot_shift, rot_origin, rot_matrix)
    
    
def get_chimera_rot_axis(session, template1_id, template2_id):
    #  Note that this is a decent approximation for the central axis of 
    #   bent actin filaments, because the subunit rotation is close to 180 degrees.
    
    # Make sure template models are visible, or else measure rotation will fail to
    #  generate the axis model!
    run(session, 'sel %s|%s; show sel m; rib sel; ~sel sel' %
        (template1_id, template2_id))
    
    model_list1 = [m.id for m in session.models]
    run(session, 'wait 1; measure rotation %s to %s' %
        (template1_id, template2_id))
    model_list2 = [m.id for m in session.models]
    rot_axis_id = '#%s' % (list(set(model_list2) - set(model_list1))[0][0])
    run(session, '~show sel m; ~rib sel; ~sel sel; wait 1')

    rot_axis = np.zeros([2, 3])
    rot_axis_x1 = run(session, 'getcrd %s:1' % (rot_axis_id))
    rot_axis[0,:] = rot_axis_x1[0]
    rot_axis_x2 = run(session, 'getcrd %s:2' % (rot_axis_id))
    rot_axis[1,:] = rot_axis_x2[0]
    # rot_axis /= np.linalg.norm(rot_axis)

    ####################
    # Get axis angle and shift
    ####################
    
    unit_vec = rot_axis[0] - rot_axis[1]
    unit_vec /= np.linalg.norm(unit_vec)
    
    first_atom = run(session, 'getcrd %s' % template1_id, log=False)
    first_atom = first_atom[0]
    second_atom = run(session, 'getcrd %s' % template2_id, log=False)
    second_atom = second_atom[0]

    first_proj = rot_axis[0] + \
        unit_vec * \
        np.dot(unit_vec, first_atom - rot_axis[0])

    second_proj = rot_axis[0] + \
        unit_vec * \
        np.dot(unit_vec, second_atom - rot_axis[0])

    dist = np.linalg.norm(second_proj - first_proj)
    angle = 180/np.pi * np.arccos(np.dot( (first_atom - first_proj) /
                                          np.linalg.norm(first_atom - first_proj), 
                                          (second_atom - second_proj) / \
                                          np.linalg.norm(second_atom - second_proj)))
    
    run(session, 'close %s' % rot_axis_id)

    return(rot_axis, angle, dist)
    

def register_command(session):
    from chimerax.core.commands import CmdDesc, register
    from chimerax.atomic import StructureArg, AtomsArg

    from chimerax.core.commands import CmdDesc, register, FloatArg, BoolArg, StringArg, IntArg, SaveFileNameArg
    
    desc = CmdDesc(
        required = [('from_atoms', StringArg),],
        keyword = [('to_atoms', StringArg),
                   ('color',StringArg),
                   ('model_id',StringArg),
                   ('radius',FloatArg),
                   ('vector_scale',FloatArg),
                   ('molmap_res', FloatArg),
                   ('molmap_transparency', FloatArg),
                   ('molmap_level', FloatArg),
                   ('com_radius',FloatArg),
                   ('rotvec_len',FloatArg),
                   ('rotvec_radius',FloatArg),
                   ('rotvec_visual_scale',FloatArg),
                   ('arrowhead_radius',FloatArg),
                   ('arrowhead_height',FloatArg),],
        url = 'https://gitlab.com/cvsindelar/subdomain_transforms', 
        synopsis = 'copy atom coordinates between structures')
    register('subdomain_transforms', desc, subdomain_transforms, logger=session.logger)

    desc2 = CmdDesc(required= [('from_atoms', AtomsArg)],
                   synopsis = 'get ribbon colors for chains')

    register('get_chain_colors', desc2, get_chain_colors, logger=session.logger)

    desc4 = CmdDesc(required = [('model1', AtomsArg),],
                    keyword =  [('model2', AtomsArg),],
                    synopsis = 'Return rotation matrix and translation vector relating two models')
    register('get_align_info', desc4, get_align_info, logger=session.logger)

    
register_command(session)
