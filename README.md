# subdomain_transforms.py

[Usage](https://www.cgl.ucsf.edu/chimerax/docs/user/commands/usageconventions.html):

<b>subdomain_transforms</b> [from_atoms](https://www.cgl.ucsf.edu/chimerax/docs/user/selection.html) <b>to_atoms</b> [to_atoms](https://www.cgl.ucsf.edu/chimerax/docs/user/selection.html) [ <b>color</b> <i>color</i> ] [ <b>model_id</b> <i>modelid</i> ] [ <b>radius</b> <i>translation_vector_radius</i> ] [ <b>vector_scale</b> <i>vector_scale</i> ] [ <b>molmap_res</b> <i>molmap_res</i> ] [ <b>molmap_transparency</b> <i>molmap_transparency</i> ] [ <b>molmap_level</b> <i>molmap_level</i> ] [ <b>com_radius</b> <i>com_radius</i> ][ <b>rotvec_len</b> <i>rotvec_len</i> ] [ <b>rotvec_radius</b> <i>rotvec_radius</i> ] [ <b>rotvec_visual_scale</b> <i>rotvec_visual_scale</i> ] [ <b>arrowhead_radius</b> <i>arrowhead_radius</i> ] [ <b>arrowhead_height</b> <i>arrowhead_height</i> ] ]

This is a script for UCSF Chimerax that measures and visualizes subdomain rotations and translations between two PDB models.

After downloading the [python script](https://gitlab.com/cvsindelar/subdomain_transforms/-/blob/main/subdomain_transforms.py), open it in UCSF Chimera; thereafter, you may access 
this help information by typing 'help subdomain_transforms' in the command line.

To reproduce the analysis for figures in the citation below, run the UCSF ChimeraX script [subdomain_transforms_fig5.cxc](https://gitlab.com/cvsindelar/subdomain_transforms/-/blob/main/subdomain_transforms_fig5.cxc) .

This script is licensed under the terms of the GNU Public License, version 3 (GPLv3).

# How to cite
Please cite the following paper:

High resolution cryo-EM structures reveal how phosphate release from Arp3 bends and weakens Arp2/3 branch complexes. 
Sai Shashank Chavali, Steven Z. Chou, Thomas D. Pollard, Enrique M. De La Cruz, and Charles V. Sindelar. Nat. Comm. 15:2059 (2024). 

[doi: 10.1038/s41467-024-46179-x](https://doi.org/10.1038/s41467-024-46179-x). 

[PMC 10918085](http://www.ncbi.nlm.nih.gov/pmc/articles/pmc10918085/). http://www.ncbi.nlm.nih.gov/pmc/articles/pmc10918085/

# Instructions

Example:
```
open /Users/charless/scripts_chimx/subdomain_transforms.py
open 8uxx
open 8uxw
subdomain_transforms #1/A:1-38,69-173,379-419@CA to #2/A:1-38,69-173,379-419@CA vector_scale 100 color orange arrowhead_height 10 rotvec_visual_scale 1 molmap_res 25
```          

(C) 2024 Chuck Sindelar

Yale University
